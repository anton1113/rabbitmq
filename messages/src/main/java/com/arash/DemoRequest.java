package com.arash;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by anton on 19.07.18.
 *
 */
@Data
public class DemoRequest implements Cloneable, Serializable {

    private String requestText;
}
