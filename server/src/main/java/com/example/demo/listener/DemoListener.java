package com.example.demo.listener;

import com.arash.DemoRequest;
import com.arash.DemoResponse;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Created by anton on 17.07.18.
 *
 */
@Component
public class DemoListener {

    @RabbitListener(queues = "demo")
    public DemoResponse onDemoMessageReceived(DemoRequest request) {
        String requestText = request.getRequestText();
        String responseText = "Response: " + requestText;
        DemoResponse response = new DemoResponse();
        response.setResponseText(responseText);
        return response;
    }
}
