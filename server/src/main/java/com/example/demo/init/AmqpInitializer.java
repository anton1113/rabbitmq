package com.example.demo.init;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;

/**
 * Created by anton on 17.07.18.
 *
 */
@Component
public class AmqpInitializer {

    @Autowired private AmqpAdmin amqpAdmin;

    @PostConstruct
    private void init() {
        amqpAdmin.declareExchange(new DirectExchange("demo"));
        amqpAdmin.declareQueue(new Queue("demo"));
        amqpAdmin.declareBinding(new Binding("demo", Binding.DestinationType.QUEUE,
                "demo", "demo", Collections.emptyMap()));
    }
}
