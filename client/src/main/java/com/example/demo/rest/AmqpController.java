package com.example.demo.rest;

import com.arash.DemoRequest;
import com.arash.DemoResponse;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by anton on 17.07.18.
 *
 */
@RestController
public class AmqpController {

    @Autowired private RabbitTemplate rabbitTemplate;

    @RequestMapping(method = RequestMethod.POST, value = "/rest/emit/demo")
    public DemoResponse demo(@RequestBody DemoRequest request) {
        DemoResponse response = (DemoResponse) rabbitTemplate.convertSendAndReceive("demo", "demo", request);
        return response;
    }
}
